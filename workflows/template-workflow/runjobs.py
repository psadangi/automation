#!/usr/bin/env python3
import sys
from ecalautoctrl import HTCHandler

if __name__ == '__main__':
    handler = HTCHandler(task='MYAPP', # MYAPP must be equal to the name of the workflow you create
                         pd='/MYPD',
                         tier='RAW')

    ret = handler(wflow='my-workflow')

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(HTCHandler)
